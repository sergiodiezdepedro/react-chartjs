import React, {Component} from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';

class Chart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: props.chartData
    }
  }

  static defaultProps = {
    displayTitle: true,
    displayLegend: true,
    legendPosition: 'top',
    estado: 'estado'
  }

  render() {
    return (
      <div className="chart-container">
        <Bar data={this.state.chartData} options={{
          responsive: true,
          maintainAspectRatio: false,
          title: {
            display: this.props.displayTitle,
            text: 'Las ciudades más pobladas del estado de ' + this.props.estado + ' (salvo NYC)',
            fontSize: 25
          },
          legend: {
            display: this.props.displayLegend,
            position: this.props.legendPosition
          }
        }}
        />
        <Line data={this.state.chartData} options={{
          responsive: true,
          maintainAspectRatio: false,
          title: {
            display: this.props.displayTitle,
            text: 'Las ciudades más pobladas del estado de ' + this.props.estado + ' (salvo NYC)',
            fontSize: 25
          },
          legend: {
            display: this.props.displayLegend,
            position: this.props.legendPosition
          }
        }}
        />
        <Pie data={this.state.chartData} options={{
          responsive: true,
          maintainAspectRatio: false,
          title: {
            display: this.props.displayTitle,
            text: 'Las ciudades más pobladas del estado de ' + this.props.estado + ' (salvo NYC)',
            fontSize: 25
          },
          legend: {
            display: this.props.displayLegend,
            position: this.props.legendPosition
          }
        }}
        />
      </div>
    )
  }
}

export default Chart;
